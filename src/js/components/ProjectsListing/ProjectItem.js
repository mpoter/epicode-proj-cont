import React from "react";
import { Link } from "react-router";
import * as ProjectActions from "../../actions/ProjectActions"

export default class ProjectItem extends React.Component {
    render(){

        const {endDate} = this.props;
        const {location} = this.props;
        const {projectId} = this.props;
        const {projectName} = this.props;
        const {startDate} = this.props;
        const {procedures} = this.props;

        const editElementRoute = "editProject/"+ projectId;
        const projectDetailsRoute = "project/"+ projectId;
        
        //const Procedures = procedures.map((procedure, i) => <p key={i}>{procedure.name}</p> );
        
        return (
            <div >
                
                <Link to={projectDetailsRoute}><h1>{projectName}</h1></Link>
                
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Start date</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" value={startDate} disabled/>
                        </div>

                        <label class="control-label col-sm-3">End date</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" value={endDate} disabled/>
                        </div>

                        <label class="control-label col-sm-3">Realization place</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" value={location} disabled/>
                        </div>
                    </div>                   
                </form>       
            </div>
        )
    }
}