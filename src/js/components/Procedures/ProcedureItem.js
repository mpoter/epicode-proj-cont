import React from "react";

export default class ProcedureItem extends React.Component {
    render(){
        const {name} = this.props.procedure;
        const prodecureSteps = this.props.procedure.steps.map(
            (step, i) =>
                <div class="checkbox" key={i}>
                    <label>
                        <input  type="checkbox" defaultChecked={step.done} disabled/>
                    {step.name}</label>
                </div>
            );
            return (
                <div>
                    <p>{name}</p>
                    {prodecureSteps}
                </div>
            )
    }
}