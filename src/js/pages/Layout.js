import React from "react";
import { Link } from "react-router";
import Nav from "../components/Layout/Nav";
import Procedures from "./Procedures";


export default class Layout extends React.Component {
    navigateToMainPage(){
        this.props.history.pushState(null, "/");
    }   

    componentWillMount(){
        //saving procedures on layout loaded
        const procedures = new Procedures;
        const itemToSave = JSON.stringify(procedures.getProcedures());
        localStorage.setItem("procedures", itemToSave);
    }
    
    render(){
        console.log()
        const { location } = this.props;
        const containerStyle = {
          marginTop: "60px"
        };

        return (
            <div>                
                <Nav location={location} />
                <div class="container" style={containerStyle}>
                    {this.props.children}
                </div>
            </div>
        )
    }
}