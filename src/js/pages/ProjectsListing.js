import React from "react";
import { Link } from "react-router";
import ProjectItem from "../components/ProjectsListing/ProjectItem";
import * as ProjectActions from "../actions/ProjectActions"
import ProjectsStore from "../stores/ProjectsStore";


export default class ProjectsListing extends React.Component {
    constructor(){
        super();
        
        this.getProjects = this.getProjects.bind(this);
        this.getProjectsByProp = this.getProjectsByProp.bind(this);
        this.state = {
            projects: ProjectsStore.getAll(),
        }
    }

    componentWillMount(){
        ProjectsStore.on("change", this.getProjects);
        ProjectsStore.on("prop-change", this.getProjectsByProp);
    }

    // to prevent memory leaks
    componentWillUnmount(){
        ProjectsStore.removeListener("change", this.getProjects);
        ProjectsStore.removeListener("prop-change", this.getProjectsByProp);
    }

    getProjects(){
        this.setState({
            projects: ProjectsStore.getAll(),
        });
    }

    getProjectsByProp(){
        this.setState({
            projects: ProjectsStore.getProjectsPropValue(),
        });
    }

    filterProjects(event){
        ProjectActions.getFilteredProjects(event.target.value);
    }

    render(){
        const { projects } = this.state;
        const pageTitle = 'Projects';

        const WrappedLink = () => {
            return (
              <button style={{margin: '0 0 50px', display: 'inline'}} class="btn btn-default">
                <Link to="createNewProject" style={{display: 'block', height: '100%'}}>CreateNewProject</Link>
              </button>
            )
          }

        if(projects == null || projects.length == 0){
            return (
                <div>
                    <h1>{pageTitle}</h1>
                    <WrappedLink/>
                    <input type="text" class="form-control" placeholder="Search project" onChange={this.filterProjects.bind(this)}/>
                    <p>No projects :(</p>
                </div>
            )
        }
        else {
            const Projects = projects.map((project, i) => <ProjectItem key={project.id} project={project} projectId={project.id} projectName={project.name} startDate={project.startDate} endDate={project.endDate} location={project.location} procedures={project.procedures} defects={project.defects}/>);
            return (
                <div>
                    <h1>{pageTitle}</h1>
                    <WrappedLink/>
                    <input type="text" class="form-control" placeholder="Search project..." onChange={this.filterProjects.bind(this)}/>
                    <div>
                        {Projects}
                    </div> 
                </div>
            )
        }
        
    }
}