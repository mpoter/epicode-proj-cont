import React from "react";
import * as ProjectActions from "../actions/ProjectActions";

export default class AddDefect extends React.Component {
    constructor(){
        super();

        this.state = {
            description: '',
            status: 'actual',
            photoData: '',
        }

        this.handleTextareaChange = this.handleTextareaChange.bind(this);
        this.handlePhotoChange = this.handlePhotoChange.bind(this);
    }

    handleTextareaChange(event) {
        this.setState({
          description: event.target.value,
        });
      }

    handlePhotoChange(event) {
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {               
                this.setState({
                    photoData: e.target.result,
                    });
            }.bind(this);  
            reader.readAsDataURL(event.target.files[0]);
        }    
    }

    addDefect(){
        console.log()
        ProjectActions.addProjectDefect(this.props.params.project, this.state.description, this.state.status, this.state.photoData);
        this.props.history.replaceState(null, "/");
    }

    render(){
        return (
            <div>
                <h1>Add defect</h1>
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Defect description</label>
                        <div class="col-sm-9">
                            <textarea name="description" class="form-control" rows="5" value={this.state.description} onChange={this.handleTextareaChange}></textarea>
                        </div>
                        <label class="control-label col-sm-3">Photo</label>
                        <div class="col-sm-9">
                            <input type="file" name="photo" class="form-control" onChange={this.handlePhotoChange}></input>
                            <img id="uploadedImage" src={this.state.photoData} />
                        </div>                           
                    </div>
                    <div style={{float: 'right'}}>
                        <button class="btn btn-default" onClick={this.addDefect.bind(this)}>Add defect</button>                  
                    </div>                  
                </form>
            </div>
        )
    }
}