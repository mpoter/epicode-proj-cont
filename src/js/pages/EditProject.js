import React from "react";
import ProjectsStore from "../stores/ProjectsStore";
import * as ProjectActions from "../actions/ProjectActions";

export default class EditProject extends React.Component {
    constructor(){
        super();

        this.state = {
            id: '',
            name: '',
            startDate: '',
            endDate: '',
            location: '',
            procedures: [],
            affairs: []            
        }
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    getEditedProjectData(){
        var currentProjectData = ProjectsStore.getAll().filter(project => project.id == this.props.params.project);

        for(var key in currentProjectData[0]){
            this.setState({
                [key]: currentProjectData[0][key],
            })
        }
    }

    componentWillMount(){
        this.getEditedProjectData();
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
    
        this.setState({
          [name]: value
        });
      }

    navigateToMainPage(){
        this.props.history.replaceState(null, "/");
    }

    editProject(){
        ProjectActions.editProject(this.state.id, this.state.name, this.state.startDate, this.state.endDate, this.state.location, this.state.procedures, this.state.affairs);
        this.navigateToMainPage();
    }

    render(){
        return (
            <div>
                <h1>Edit project</h1>
                <form>
                    <input name="name" type="text" placeholder="Name" class="form-control" value={this.state.name} onChange={this.handleInputChange} />
                    <input name="startDate" type="date" placeholder="Start date" class="form-control" value={this.state.startDate} onChange={this.handleInputChange} />
                    <input name="endDate" type="date" placeholder="End date" class="form-control" value={this.state.endDate} onChange={this.handleInputChange} />
                    <input name="location" type="text" placeholder="Location" class="form-control" value={this.state.location} onChange={this.handleInputChange} />
                </form>
                <button onClick={this.editProject.bind(this)} class="btn btn-default">Save changes</button>
            </div>
        )
    }
}