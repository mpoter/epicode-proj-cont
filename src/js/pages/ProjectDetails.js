import React from "react";
import ProjectsStore from "../stores/ProjectsStore";
import * as ProjectActions from "../actions/ProjectActions";
import ProcedureItem from "../components/Procedures/ProcedureItem";
import { Link } from "react-router";

export default class ProjectDetails extends React.Component {
    constructor(){
        super();

        this.state = {
            id: '',
            name: '',
            startDate: '',
            endDate: '',
            location: '',
            procedures: [],
            defects: []            
        }
    }

    getEditedProjectIdFromLink(){
        const linkLastChild = window.location.href.split("/").pop();
        const linkSplitted = linkLastChild.split("?", 1);

        return linkSplitted;
    }

    getEditedProjectData(){

        var allProjects = ProjectsStore.getAll();
        var currentProjectData = allProjects.filter(project => project.id == this.getEditedProjectIdFromLink());

        this.setState({
            id: currentProjectData[0].id,
            name: currentProjectData[0].name,
            startDate: currentProjectData[0].startDate,
            endDate: currentProjectData[0].endDate,
            location: currentProjectData[0].location,
            procedures: currentProjectData[0].procedures,
            defects: currentProjectData[0].defects,
        });
    }

    componentWillMount(){
        this.getEditedProjectData();
    }

    navigateToMainPage(){
        this.props.history.replaceState(null, "/");
    }

    addPorcedure(procedureId){
        ProjectActions.addProcedureToProject(procedureId, this.state.id);
        window.location.reload();
    }

    deleteProject(){
        if (confirm("Do you want to delete this project?") == true) {
            ProjectActions.deleteProject(this.state.id);
            this.navigateToMainPage();
        } else {
            return false;
        }
    }

    deleteDefect(event){
        ProjectActions.deleteProjectDefect(this.state.id, event.target.dataset.index);
        window.location.reload();

    }
    
    render(){

        const imgSize = {
            maxWidth: '100%',
            maxHeight: '100%'
        };

        const editElementRoute = "editProject/"+ this.getEditedProjectIdFromLink();
        const addDefectRoute = "addDefect/"+ this.getEditedProjectIdFromLink();
        const Procedures = this.state.procedures.map((procedure, i) => <ProcedureItem key={procedure.id} procedure={procedure} /> );
        const Defects = this.state.defects.map((defect, i) =>
          <tr key={i}>
            <td>{defect.description}</td>
            <td>{defect.status}</td>
            <td><img src={defect.photoData} style={imgSize}/></td>
            <td>
                <button class="btn btn-default" data-index={i} onClick={this.deleteDefect.bind(this)}>Delete</button>  
            </td>
          </tr>
    );

        return (
            <div>
                <h1>Project {this.state.name} details</h1>
            
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" value={this.state.name} disabled/>
                        </div>

                        <label class="control-label col-sm-3">Start date</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" value={this.state.startDate} disabled/>
                        </div>

                        <label class="control-label col-sm-3">End date</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" value={this.state.endDate} disabled/>
                        </div>

                        <label class="control-label col-sm-3">Realization place</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" value={this.state.location} disabled/>
                        </div>
                        
                    </div>

                    <div style={{float: 'right'}}>
                        <button onClick={this.deleteProject.bind(this)} class="btn btn-default">Delete</button>
                        <button class="btn btn-default"><Link to={editElementRoute}>Edit Project</Link></button> 
                        <button class="btn btn-default"><Link to={addDefectRoute}>addDefect</Link></button>                  
                    </div>                  
                </form>
                <button onClick={this.addPorcedure.bind(this, 1)} class="btn btn-default">Add prodedure #1</button>
                <button onClick={this.addPorcedure.bind(this, 2)} class="btn btn-default">Add procedure #2</button>

                <div>
                    <h3>Procedures assigned</h3>
                    {Procedures}
                </div>


                <h2>Defects</h2>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Photo</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        {Defects}
                    </tbody>
                </table>
            </div>
        )
    }
}