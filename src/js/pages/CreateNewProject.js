import React from "react";
import * as ProjectActions from "../actions/ProjectActions"

export default class CreateNewProject extends React.Component {
    constructor(){
        super();

        this.state = {
            projectName: '',
            startDate: '',
            endDate: '',
            location: '',
            procedures: [],
            defects: []            
        }
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
    
        this.setState({
          [name]: value
        });
      }

    navigateToMainPage(){
        this.props.history.replaceState(null, "/");
    }

    createProject(){
        ProjectActions.createProject(this.state.projectName, this.state.startDate, this.state.endDate, this.state.location);
        this.navigateToMainPage();
    }

    render(){
        return (
            <div class="row">
                <h1>Create new project</h1>

                <form class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-3">Name</label>
                    <div class="col-sm-9">
                        <input name="projectName" type="text" class="form-control" value={this.state.projectName} onChange={this.handleInputChange}/>
                    </div>

                    <label class="control-label col-sm-3">Stat date</label>
                    <div class="col-sm-9">
                        <input name="startDate" type="date" class="form-control" value={this.state.startDate} onChange={this.handleInputChange}/>
                    </div>

                    <label class="control-label col-sm-3">End date</label>
                    <div class="col-sm-9">
                        <input name="endDate" type="date" class="form-control" value={this.state.endDate} onChange={this.handleInputChange}/>
                    </div>

                    <label class="control-label col-sm-3">Realization place</label>
                    <div class="col-sm-9">
                        <input name="location" type="text" class="form-control" value={this.state.location} onChange={this.handleInputChange}/>
                    </div>
                </div>
                </form>
                
                <button onClick={this.createProject.bind(this)} style={{float: 'right'}} class="btn btn-default">CreateProject</button>
            </div>
        )
    }
}