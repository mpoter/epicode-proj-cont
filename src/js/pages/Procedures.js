import React from "react";
import ProcedureItem from "../components/Procedures/ProcedureItem"

export default class Procedures extends React.Component {
    constructor(){
        super();
        
        this.getProcedures = this.getProcedures.bind(this);
        this.procedures = [
            {
                id: 1,
                name: 'Procedura budowy fundamentów',
                steps: [
                    {name: 'Wykop wykonany na głębokość minimum 1m', done: false},
                    {name: 'Wykop wykonany na szerokość od 50 cm do 100 cm', done: false},
                    {name: 'Brak wód gruntowych', done: true},
                ]
            },
            {
                id: 2,
                name: 'Procedura budowy dachu',
                steps: [
                    {name: 'Konstrukcja zgodna z projektem', done: true},
                    {name: 'Belki zabezpieczone przed wilgocią', done: true},
                    {name: 'Nachylenie dachu przynajmniej 10%', done: true},
                ]
            }
        ];
    }

    getProcedures(){
        return this.procedures;
    }


    render(){

        const Procedures = this.procedures.map((procedure) => <ProcedureItem key={procedure.id} procedure={procedure} />);
        return (
            <div class="container">
                <h1>Procedures</h1>
                <form>
                    {Procedures}
                </form>
            </div>
        )
    }
}