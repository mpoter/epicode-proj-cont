import dispatcher from "../dispatcher";

export function createProject(name, startDate, endDate, location) {
    dispatcher.dispatch({
        type: "CREATE_PROJECT",
        name,
        startDate,
        endDate,
        location
    });
}

export function deleteProject(id){
    dispatcher.dispatch({
        type: "DELETE_PROJECT",
        id
    });
}

export function editProject(id, name, startDate, endDate, location, procedures, affairs){
    dispatcher.dispatch({
        type: "EDIT_PROJECT",
        id,
        name,
        startDate,
        endDate,
        location,
        procedures,
        affairs
    });
}

export function addProcedureToProject(id, projectId){
    dispatcher.dispatch({
        type: "ADD_PROCEDURE_TO_PROJECT",
        id,
        projectId
    });
}

export function getFilteredProjects(searchedQuery){
    dispatcher.dispatch({
        type: "GET_FILTERED_PROJECTS",
        searchedQuery
    });
}

export function addProjectDefect(id, description, status, photoData){
    dispatcher.dispatch({
        type: "ADD_PROJECT_DEFECT",
        id,
        description,
        status,
        photoData
    });
}

export function deleteProjectDefect(id, defectIndex){
    dispatcher.dispatch({
        type: "DELETE_PROJECT_DEFECT",
        id,
        defectIndex
    });
}