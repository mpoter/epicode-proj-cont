import { EventEmitter } from "events";
import dispatcher from "../dispatcher";

class ProjectsStore extends EventEmitter {
    constructor(){
        super();

        if(this.getFromLocalStorage() == null)
        {
            this.projects = [];
        }else{
            this.projects = this.getFromLocalStorage();
        }
        
    }

    saveToLocalStorage(item, name){
        const itemToSave = JSON.stringify(item);
        localStorage.setItem(name, itemToSave);
    };

    getFromLocalStorage(){
        const projects = localStorage.getItem("projects");
        return JSON.parse(projects);
    };

    getAll() {
        return this.getFromLocalStorage();
    };

    getProjectsPropValue(){
        return this.projects;
    }

    getAllProcedures(){
        const procedures = localStorage.getItem("procedures");
        return JSON.parse(procedures);
    }
    getProcedure(id){
        const searchedProcedure = JSON.parse(localStorage.getItem("procedures")).filter(procedure => procedure.id == id);
        return searchedProcedure;
    }

    getFilteredProjectsList(searchedQuery){

        const baseProjecsList = this.getAll();
        const filteredProjectsList = this.getAll().filter((project) => project.name.includes(searchedQuery));

        if(searchedQuery.length != 0 ){
            this.projects = filteredProjectsList;
        }else{
            this.projects = baseProjecsList;
        }
        this.emit("prop-change");
    }

    addProjectDefect(id, description, status, photoData){
        const defectData = {description, status, photoData};
        const searchedProjectIndex = this.getAll().findIndex(project => project.id == id);
        
        const projectsList = this.getAll();
        projectsList[searchedProjectIndex].defects.push(defectData);

        this.projects = projectsList;
        this.emit("change");
    }

    deleteProjectDefect(id, defectIndex){
        const searchedProjectIndex = this.getAll().findIndex(project => project.id == id);

        const projectsList = this.getAll();
        projectsList[searchedProjectIndex].defects.splice(defectIndex, 1);

        this.projects = projectsList;

        this.emit("change");
    }


    // ===== Start of projects crud methods
    createProject(name, startDate, endDate, location){
        const id = Date.now();
        this.projects.push({
            id,
            name,
            startDate,
            endDate,
            location,
            procedures: [],
            defects: []
        });

        this.emit("change");
    };

    deleteProject(id){
        var updatedProjectsList = this.getAll().filter(function(el) {
            return el.id !== id;
        });
        this.projects = updatedProjectsList;

        this.emit("change");
    };
    

    editProject(id, name, startDate, endDate, location, procedures, affairs){

        const receivedObject = {id, name, startDate, endDate, location, procedures, affairs};
        const searchedObjectIndex = this.getAll().findIndex(obj => obj.id == id);

        const projectsList = this.getAll();
        for(var key in projectsList[searchedObjectIndex]){
            projectsList[searchedObjectIndex][key] = receivedObject[key]
        }

        this.projects = projectsList;
        this.emit("change");
    };
    // ======= End of projects crud methods


    addProcedureToProject(id, projectId){

        console.log(id);

        var allProjects = this.getAll();

        const projectsWithoutReveivedOne = allProjects.filter(obj => obj.id !== projectId);
        const destinedProject = allProjects.filter(project => project.id == projectId);

        var prodedureToBeInserted = this.getProcedure(id);

        destinedProject[0].procedures.push(prodedureToBeInserted[0]);
        projectsWithoutReveivedOne.push(destinedProject[0]);

        this.projects = projectsWithoutReveivedOne;

        this.emit("change");
    }


    handleActions(action){
        switch(action.type){
            case "CREATE_PROJECT": {
                this.createProject(action.name, action.startDate, action.endDate, action.location);
                this.saveToLocalStorage(this.projects, "projects");
                break;
            }
            case "DELETE_PROJECT": {
                this.deleteProject(action.id);
                this.saveToLocalStorage(this.projects, "projects");
                break;
            }
            case "EDIT_PROJECT": {
                this.editProject(action.id, action.name, action.startDate, action.endDate, action.location, action.procedures, action.affairs);
                this.saveToLocalStorage(this.projects, "projects");
                break;
            }
            case "ADD_PROCEDURE_TO_PROJECT": {
                this.addProcedureToProject(action.id, action.projectId);
                this.saveToLocalStorage(this.projects, "projects");
                break;
            }
            case "GET_FILTERED_PROJECTS": {
                this.getFilteredProjectsList(action.searchedQuery);
                break;
            }
            case "ADD_PROJECT_DEFECT": {
                this.addProjectDefect(action.id, action.description, action.status, action.photoData);
                this.saveToLocalStorage(this.projects, "projects");
                break;
            }
            case "DELETE_PROJECT_DEFECT": {
                this.deleteProjectDefect(action.id, action.defectIndex);
                this.saveToLocalStorage(this.projects, "projects");
                break;
            }
        }
    }

};

const projectsStore = new ProjectsStore;
dispatcher.register(projectsStore.handleActions.bind(projectsStore));

export default projectsStore;