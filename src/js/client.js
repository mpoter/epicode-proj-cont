import React from "react";
import ReactDOM from "react-dom";
import { Router, Route, IndexRoute, hashHistory } from "react-router";

import Layout from "./pages/Layout";
import Procedures from "./pages/Procedures";
import ProjectsListing from "./pages/ProjectsListing";
import CreateNewProject from "./pages/CreateNewProject";
import EditProject from "./pages/EditProject";
import ProjectDetails from "./pages/ProjectDetails";
import AddDefect from "./pages/AddDefect";

const app = document.getElementById('app');

ReactDOM.render(
    <Router history={hashHistory}>
        <Route path="/" component={Layout}>
            <IndexRoute component={ProjectsListing}></IndexRoute>
            <Route path="/project/:projectDetails" component={ProjectDetails}></Route>
            <Route path="/addDefect/:project" component={AddDefect}></Route>
            <Route path="procedures" component={Procedures}></Route>
            <Route path="createNewProject" component={CreateNewProject}></Route>
            <Route path="editProject/:project" component={EditProject}></Route>
        </Route>
    </Router>,
  app);